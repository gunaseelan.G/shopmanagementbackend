package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Products;
import com.example.demo.repository.ProductRepository;

@RestController
@CrossOrigin(origins = "http://localhost:4200/")
public class ProductController {
	
    @Autowired
    private ProductRepository productRepository;
    
    @GetMapping("/get")
    public  List<Products> getproduct(){
    	return productRepository.findAll();
    }
}
