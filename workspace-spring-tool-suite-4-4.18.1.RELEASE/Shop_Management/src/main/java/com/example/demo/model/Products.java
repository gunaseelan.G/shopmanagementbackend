package com.example.demo.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "product")
public class Products {
	@Id
	@GeneratedValue
	private int id;

	@Column(name = "productname")
	private String productname;

	@Column(name = "price")
	private int price;

	@Column(name = "image")
	private String image;

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Products(int id, String productname, int price, String image) {
		super();
		this.id = id;
		this.productname = productname;
		this.price = price;
		this.image = image;
	}

	public Products() {
		super();
	}

}
