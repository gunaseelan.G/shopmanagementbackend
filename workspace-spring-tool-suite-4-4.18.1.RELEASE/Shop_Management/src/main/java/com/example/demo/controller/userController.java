package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

//import com.example.demo.model.AdminLog;
import com.example.demo.model.User;
import com.example.demo.repository.UserObjectRepository;
//import com.example.demo.repository.UserRepository;
import com.example.demo.services.UserObjectService;
//import com.example.demo.services.AdminService;
//import com.example.demo.services.UserService;
//import com.example.demo.services.UserServiceImple;

@RestController
@CrossOrigin(origins = "*")
public class userController {

	
	@Autowired
	private UserObjectService userObjectService;
	
	@Autowired
	private UserObjectRepository userrepo;
	


	@GetMapping("/user")
	public List<User> getUser() {
		return userObjectService.getUser();
	}
	
	@PostMapping("/user")
	public User postUser(@RequestBody User user){
		return userObjectService.postUser(user);
	}	
	
	
//	@GetMapping("/get")
//	public List<Object> getIdUser() {
//		return userObjectService.getId();
//	}
	
	
	
	@GetMapping("/userObject/{Username}")
	public List <Object> getsem(@PathVariable("Username")String Username){
		int id=userObjectService.getdata(Username);
		List<Object> data = userObjectService.getOne1(id);
		return data;
	}
	
	@PostMapping("/Login")
	public ResponseEntity<User>login(@RequestBody User loginuser){
	User user = userObjectService.log(loginuser);
	
	if(user.getPassword().equals(loginuser.getPassword()))
    return ResponseEntity.ok(user);
	return (ResponseEntity<User>) ResponseEntity.internalServerError() ;
	}
	
	
//	
//	@PostMapping("/user")
//	public User postUser(@RequestBody User user) {
//		return userService.postUser(user);
//	}
//	
	
//	@GetMapping("/user/{username}/{password}")
//	public int login(@PathVariable("username") String username,@PathVariable("password") String password) {
//		
//		int flag =userService.UserLogin(username,password);
//		if(flag==0) {
//			return 0;
//		}
//		return flag;
//	}
//        @PostMapping("/admin")
//        public ResponseEntity<User>get(@RequestBody User admin){
//        	User user=adminService.login(admin);
//        	System.out.println(user);
//        	if(user.getPassword().equals(admin.getPassword()))
//        		return ResponseEntity.ok(user);
//        	return (ResponseEntity<User>) ResponseEntity.internalServerError();
//        }
	
//	login user getting process----------

//	@PostMapping("/user/login")
//	public ResponseEntity<User> loginAdmin(@RequestBody User userData){
//		User user= userServiceImple.login(userData);
//		System.out.println(user);
//		if(user.getPassword().equals(userData.getPassword()))
//			return ResponseEntity.ok(user);
//		return (ResponseEntity<User>) ResponseEntity.internalServerError(); 
//	}

	
}
