package com.example.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.demo.model.User;
import com.example.demo.repository.UserObjectRepository;
import com.example.demo.repository.UserRepository;
@Service
public class UserObjectService {
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserObjectRepository userObjectRepository;
	
	public List <Object> getOne1(int id){
		return userRepository.get(id);
	}
	
	public User postUser(User us) {
		return userObjectRepository.save(us);
	}
	

//	public ResponseEntity<User> postUser1(User user) {
//		User user1 = userObjectRepository.save(user);
//		return ResponseEntity.ok(user1);
//	}
	
	public List<User> getUser() {
		return userObjectRepository.findAll();
	}
	public int getdata(String username) {
		User user=userObjectRepository.findByUserName(username);
		int id =0;s
		if(user!=null) {
			id=user.getId();
			return id;
		}else {
			return 0;
		}
	}
	
	public User log(User loginuser ) {
		User user = userObjectRepository.findByUserName(loginuser.getUserName());
		return user;
	}
	
	
	
//
//	public ResponseEntity<User> log(User loginuser) {
//		User user=userObjectRepository.findByUserName(loginuser.getUserName());
//		if(user!=null && user.getPassword()!=null && user.getPassword().equals(loginuser.getPassword()))
//			return ResponseEntity.ok(user);
//		else {
//			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
//			
//
//	}

	
	
//	public List<Object> getId(){
//		return userRepository.getN();
//	}
	}

	
	
