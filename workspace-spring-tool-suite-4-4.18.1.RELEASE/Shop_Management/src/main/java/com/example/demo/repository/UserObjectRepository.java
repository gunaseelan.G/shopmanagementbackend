package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.User;

//import com.example.demo.model.User;

@Repository
public interface UserObjectRepository extends JpaRepository<User,Integer> {
	
	User findByUserName(String userName);

}
